from abc import ABC, abstractmethod


def snake_case(x: str):
    return x.lower().strip().replace(" ", "_")


def count_passing_tests(pytest_str: str):
    lines = [
        line.strip() for line in pytest_str.splitlines(False) if line.strip() != ""
    ]
    final_line = next(reversed(lines))
    results = final_line.split()
    try:
        pindex = results.index("passed")
        return int(results[pindex - 1])
    except ValueError:
        return 0


class Animal(ABC):
    @abstractmethod
    def sound(self):
        raise NotImplementedError


class Dog(Animal):
    def sound(self):
        print("bark")


class Cat(Animal):
    def sound(self):
        print("meow")


def polymorphism_example():
    print(1 + 1)  # 2
    print("string1" + "string2")


def poly_classes():
    for item in (Cat(), Dog()):
        item.sound()


def poly_append_example(x):
    x.append("hello")
    return x


class Foo(object):
    def append(self, v):
        print("foo.append", v)


if __name__ == "__main__":
    # polymorphism_example()
    # poly_classes()
    poly_append_example(Foo())
    print(poly_append_example(["item1"]))
