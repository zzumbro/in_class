from datetime import datetime, timedelta

from sql_db import Base
from sqlalchemy import Column, Integer, String


class User:
    def __init__(self, username):
        logger.info(f"creating a new user {username}")
        self.username: str = username

    def to_upper(self):
        return self.username.upper()

    def to_lower(self):
        return self.username.lower()


class Student(Base):
    __tablename__ = "student"

    student_id: int = Column(Integer, primary_key=True)
    name: str = Column(String)
    email: str = Column(String)

    def __repr__(self):
        return f"<Student {self.email}>"


if __name__ == "__main__":
    from loguru import logger

    u = User("username1")
    s = Student(1, "student1")
    s2 = Student(2, "student2")
    logger.info(u.to_upper())
    logger.warning(s.to_lower())
    logger.info(s.student_dictionary())
    logger.error(s2.student_dictionary())
    logger.debug(s.status)
