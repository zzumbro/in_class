from mongo_db import instance
from umongo import Document
from umongo.fields import IntField, StrField


@instance.register
class House(Document):
    color: str = StrField(required=True)
    length: int = IntField(required=True)
    width: int = IntField(default=10)
    # foundatation
    # materials
    # employees
    # color
    # def __init__(self, employees, color, length=100, width=100):
    #     self.employees = employees
    #     self.color = color
    #     self.length = length
    #     self.width = width

    def __repr__(self):
        return f"<House {self.color}>"

    @property
    def __dict__(self):
        return {
            "employees": self.employees,
            "color": self.color,
            "length": self.length,
            "width": self.width,
            "square_footage": self.square_footage(),
        }

    def square_footage(self):
        return self.length * self.width


def square_footage(length, width):
    return length * width


if __name__ == "__main__":
    h = House(color="white", length=20)
    h.commit()
    # print(type(h), h)
    from pprint import pprint

    x = 1
    print(id(x))
    pprint([dict(h.dump()) for h in House.find({})])
