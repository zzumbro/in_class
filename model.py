class Celsius:
    def __init__(self, temperature=0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value...")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        print("Setting value...")
        if value < -273.15:
            raise ValueError("Temperature below -273 is not possible")
        self._temperature = value


class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password


class Student(User):
    def __init__(self, username, password, student_id):
        super().__init__(username, password)
        self.student_id = student_id
        pass


example_list = [
    Student("alex", "password", 1),
    Student("josh", "password", 2),
]


def first_repeating(x):
    s = set()
    for item in x:
        if item in s:
            return item
        s.add(x)


def second_solution(x):
    for idx1, item1 in enumerate(x):
        for idx2, item2 in enumerate(x):
            if idx1 == idx2:
                continue
            if item1 == item2:
                return


# create an object
if __name__ == "__main__":
    example_dict = {}
    for x in example_list:
        example_dict[x.student_id] = x
    print(example_dict)

    func = lambda x: x.student_id

    def func2(x: Student):
        return x.student_id

    example_dict2 = {func(x): x for x in example_list}

    x = [1, 2, 3, 4]
    y = [4, 5, 6, 7]
    z = list(set(x).intersection(set(y)))
    print(z)
