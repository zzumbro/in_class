from typing import List

import pytest


def determine_first_repeating1(input_list):
    for i, x in enumerate(input_list):
        for j, y in enumerate(input_list):
            if i == j:
                continue
            if x == y:
                return x


def determine_first_repeating(input_list):
    seen = set()
    for x in input_list:
        if x in seen:
            return x
        seen.add(x)


@pytest.mark.parametrize(
    "input_list,out", [(["a", "b", "c", "a"], "a"), (["a", "b", "b"], "b")]
)
def test_first_repeating1(input_list, out):
    assert determine_first_repeating1(input_list) == out


@pytest.mark.parametrize(
    "input_list,out", [(["a", "b", "c", "a"], "a"), (["a", "b", "b"], "b")]
)
def test_first_repeating(input_list, out):
    assert determine_first_repeating(input_list) == out
