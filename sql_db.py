import sqlite3

from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

engine = create_engine("sqlite:///data/in_class.db", echo=True)

SessionLocal = sessionmaker(bind=engine)
Base = declarative_base()

if __name__ == "__main__":
    db = SessionLocal()
    print(db)
