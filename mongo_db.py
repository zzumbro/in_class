from pymongo import MongoClient
from umongo.frameworks import PyMongoInstance

client = MongoClient(host="localhost", port=27017)

db = client["house"]

instance = PyMongoInstance(db)
