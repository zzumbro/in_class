# main application will go here
import json
import os
import pickle
from typing import List, Optional

from fastapi import FastAPI
from loguru import logger
from pydantic import BaseModel

from house.endpoints import router as house_router
from student.endpoints import router as student_router
from users.endpoints import router

app = FastAPI()

app.include_router(router, prefix="/users")
app.include_router(house_router, prefix="/house")
app.include_router(student_router)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/data")
def post_item(data: dict):
    data["message"] = "Hello!"
    return data


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


class IncomingRequest(BaseModel):
    firstname: str
    lastname: str

    def match_casing_on_first_name(self, name: str):
        return self.firstname.casefold() == name.casefold()


@app.get("/read")
def get_pickle():
    with open("./data.pkl", "r") as fp:
        data = json.load(fp)
    logger.debug(type(data))
    return data


@app.delete("/delete")
def delete(firstname: str):
    existing: List[IncomingRequest] = load_existing()
    new = [x for x in existing if not x.match_casing_on_first_name(firstname)]
    save_existing(new)


def load_existing():
    from pathlib import Path

    p = Path("./data.pkl")
    if p.exists():
        try:
            ret = []
            with open("./data.pkl", "r") as fp:
                for item in json.load(fp):
                    item: dict
                    ret.append(
                        IncomingRequest(
                            firstname=item["firstname"], lastname=item["lastname"]
                        )
                    )
                return ret
        except json.decoder.JSONDecodeError:
            return []
    return []


def save_existing(existing: List[IncomingRequest]):
    with open("./data.pkl", "w") as fp:
        json.dump([r.dict() for r in existing], fp)


@app.post("/create")
def save_pickle(req: IncomingRequest):
    existing: List[IncomingRequest] = load_existing()
    existing.append(req)
    save_existing(existing)
    return None
